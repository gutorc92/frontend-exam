/// <reference types="Cypress" />

context('Home visit', () => {
  it('Check menu', () => {
    cy.visit('/')
    cy.wait(2000)
    cy.get('#registerItem').should('be.visible')
  })
})