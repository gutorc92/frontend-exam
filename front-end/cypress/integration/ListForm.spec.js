/// <reference types="Cypress" />

context('To Do list', () => {

    it('Field fields', () => {
      cy.visit('/')
      cy.wait(2000)
      cy.get("#q-app > div > div > main > div > div.q-option-group.q-gutter-x-sm.q-option-group--inline > div:nth-child(2) > div")
        .click()
      
    })
    
    it('Mark as a done', () => {
        cy.wait(6000)
        cy.get('#q-app > div > div > main > div > div.q-option-group.q-gutter-x-sm.q-option-group--inline > div:nth-child(1) > div')
          .click()
        cy.get('#listItems > div:nth-child(1) > div.q-item__section.column.q-item__section--avatar.q-item__section--side.justify-center > div')
          .click()
        cy.get('body > div.q-notifications > div.q-notifications__list.q-notifications__list--bottom.fixed.column.items-center > div > div > div')
          .should('to.have.text', 'Item alterado com sucesso.')
    })

    it('Delete item', () => {
        cy.wait(6000)
        cy.get('#q-app > div > div > main > div > div.q-option-group.q-gutter-x-sm.q-option-group--inline > div:nth-child(1) > div')
          .click()
        cy.get('#listItems > div:nth-child(1) > div:nth-child(3) > button')
          .click()
        cy.get('body > div.q-notifications > div.q-notifications__list.q-notifications__list--bottom.fixed.column.items-center > div > div > div')
          .should('to.have.text', 'Item deletado com sucesso.')
    })

  
  })