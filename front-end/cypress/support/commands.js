// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('qFieldError', {
  prevSubject: true
}, (subject, message) => {

  // log the subject to the console
  console.log('The subject is', subject)
  return cy.wrap(subject).should('have.class', 'q-field-error')
})

Cypress.Commands.add('qSelect', {
  prevSubject: true
}, (subject, option) => {

  // log the subject to the console
  console.log('option', option)
  cy.wrap(subject).click()
  cy.get('.q-popover').contains('.q-item-main', option).click()
})

Cypress.Commands.add('qInput', {
  prevSubject: true
}, (subject, text) => {

  // log the subject to the console
  cy.wrap(subject).find('input').first().focus().type(text)
})