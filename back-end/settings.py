"""
Docstring to todo api

route: /todo
"""
from os import environ
try:
    from dotenv import load_dotenv, find_dotenv
    load_dotenv(find_dotenv())
except EnvironmentError as env_error:
    pass

# Mongo config
MONGO_HOST = environ.get("MONGO_HOST", 'localhost')
MONGO_DBNAME = environ.get("MONGO_DBNAME", 'eduqc')
# Mongo config

#Allows regex
MONGO_QUERY_BLACKLIST = ['$where']

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'DELETE', 'PUT']

RENDERERS = ['eve.render.JSONRenderer']
X_DOMAINS = '*'
X_HEADERS = ['Authorization', 'Content-type', 'Cache-Control',
             'If-Match', 'UserEmail']

PAGINATION_LIMIT = 100
PAGINATION_DEFAULT = 100


TODO_SCHEMA = {
    'id': { 'type': 'integer', 'unique': True},
    'order': { 'type': 'integer'},
    "completed": {'type': 'boolean', 'default': False}, 
    "description": {'type': 'string'}
}


DOMAIN = {
    'todo': {
        'schema': TODO_SCHEMA,
        'item_title': '',
        'resource_methods': ['GET', 'POST'],
    }
}
