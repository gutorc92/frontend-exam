import unittest
from copy import copy
from eve import Eve
import os
import json
from api import app
from datetime import datetime
import settings
from base64 import b64encode
from os import environ, path
from pymongo.errors import ServerSelectionTimeoutError

try:
    load_dotenv(find_dotenv())
except Exception as e:
    pass


class basicEveTest(unittest.TestCase):
    """Basic Eve test class
    It should let a client available and a app.
    Make requests with:

    self.get('/endpoit')

    """

    def setUp(self):
        dir_path = path.dirname(path.realpath(__file__))
        self.app = app
        self.test_client = self.app.test_client()
        hash = bytes(environ.get("MONGO_USER", '') + ':' +
                     environ.get("MONGO_PASS"), "utf-8")
        self.headers = {
            'Authorization': 'Basic %s' % b64encode(hash).decode("ascii"),
            'Content-Type': 'application/json'
        }
        self.todos = []

    def tearDown(self):
        for todo in self.todos:
            headers = copy(self.headers)
            headers['If-Match'] = todo['_etag']
            response = self.test_client.delete(
                '/todo/{}'.format(todo['_id']), headers=headers)
        self.todos = []

    def get(self, url):
        try:
            return self.test_client.get(url, headers=self.headers)
        except ServerSelectionTimeoutError as e:
            self.skipTest('Server TimeOut')

    def post(self, url, data):
        try:
            return self.test_client.post(
                url, headers=self.headers, data=data)
        except ServerSelectionTimeoutError as e:
            self.skipTest('Server TimeOut')
    
    def patch(self, url, data):
        try:
            headers = copy(self.headers)
            headers['If-Match'] = data['_etag']
            del data['_etag']
            return self.test_client.patch(
                url + '/%s' % (data['_id']), headers=headers, data=json.dumps(data))
        except ServerSelectionTimeoutError as e:
            self.skipTest('Server TimeOut')

    def delete(self, url, data):
        if '_etag' in data and '_id' in data:
            self.headers['If-Match'] = data['_etag']
            self.test_client.delete(
                '/{0}/{1}'.format(url, data['_id']), headers=self.headers)
    
    def load_response(self, response):
        data = json.loads(response.data.decode('utf-8'))
        if '_status' in data and data['_status'] == 'OK':
            return data
        elif '_items' in data:
            return data['_items']


class TestTodoSchema(basicEveTest):
    """
    Teste routes of api

    coverage run --source=. -m unittest discover -s test
    """

    url = '/todo'

    def test_post_todo(self):
        todo = {
            "completed": False,
            "description": "Testando"
        }
        response = self.post(self.url, data=json.dumps(todo))
        self.assertEqual(response.status_code, 201, response.data)
        data = self.load_response(response)
        todo.update(data)
        todo1 = {
            "completed": False,
            "description": "Testando order e id"
        }
        response = self.post(self.url, data=json.dumps(todo1))
        self.assertEqual(response.status_code, 201, response.data)
        data = self.load_response(response)
        todo1.update(data)
        with self.app.app_context():
            todo_inserted = app.data.pymongo().db.todo.find_one({"description": "Testando order e id"})
            self.assertEqual(todo_inserted['id'], 2)
            self.assertEqual(todo_inserted['order'], 2)
        self.todos.append(todo)
        self.todos.append(todo1)
    
    def test_get_todo(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_patch_todo(self):
        todo = {
            "completed": False,
            "description": "Testando"
        }
        response = self.post(self.url, data=json.dumps(todo))
        self.assertEqual(response.status_code, 201, response.data)
        with self.app.app_context():
            todo = app.data.pymongo().db.todo.find_one({"description": "Testando"})
        todo['order'] = 10
        del todo['_created']
        del todo['_updated']
        todo['_id'] = str(todo['_id'])
        response = self.patch(self.url, data=todo)
        self.assertEqual(response.status_code, 200, response.data)
        data = self.load_response(response)
        todo['_etag'] = data['_etag']
        self.todos.append(todo)

if __name__ == '__main__':
    unittest.main(verbosity=3)
