# Indrodução

O projeto foi desenvolvido em duas partes:
- front-end: 
    - desenvolvido em vue com o framework [quasar](https://quasar.dev/).
- back-end
    - api-rest desenvolvida em python com o framework [eve](https://docs.python-eve.org/en/stable/).

# Pré requisitos

Para rodar o projeto é necessário:
- node >= 9.11.2
- npm >= 5.6.0
- python > 3.5
- mongo > 4.0

O projeto ainda pode ser rodado utilizando-se docker.

# Iniciando os projetos

## Back-end

### Docker

Na pasta frontend-exam rode o comando 

`docker-compose up`

Serão criados dois containers um com o banco de dados e outro com a api.

### Sem docker

Instale as dependências do projeto listadas no arquivo requirements.txt

Inicie o projeto com o comando python api.py

## Front-end

Instale o quasar com o comando

`npm install -g @quasar/cli`

Instale os pacotes necessários dentro da pasta frond-end com o comando:

`npm install`


Inicie o projeto com o comando:

`quasar dev`

### Tests

Rode o comando:

`npm run cypress:open`

Um janela irá se abrir click no botão run all tests