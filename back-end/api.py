"""
Default route:
/history -
    stores objects with the

    user_email: email from user
    qid: question id
    scored: if the user has scored the question at the time
    subject: subject of the question
    time_spent: time pass since user has start doing the question


    see: http://python-eve.org/features.html at Filtering for more details about querys
"""
import logging
import traceback
from os import environ
from dotenv import load_dotenv, find_dotenv
from eve import Eve
from eve.auth import BasicAuth, requires_auth
from flask_cors import CORS
from flask import abort, jsonify
from eve_healthcheck import EveHealthCheck
from flask_compress import Compress
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk import configure_scope
try:
    load_dotenv(find_dotenv())
except Exception as exception:
    pass

MONGO_USER = environ.get("MONGO_USER")
MONGO_PASS = environ.get("MONGO_PASS")
SENTRY_DNS = environ.get("SENTRY_DNS", '')
SENTRY_ENV = environ.get("SENTRY_ENV", 'local')
SERVER_API = environ.get("SERVER_API", 'http://lb-api:9000/')

class MyBasicAuth(BasicAuth):
    """ Module to validate the auth user to access the API data"""

    def check_auth(self, username, password, allowed_roles, resource,
                   method):
        return username == MONGO_USER and password == MONGO_PASS

sentry_sdk.init(
    dsn=SENTRY_DNS,
    integrations=[FlaskIntegration()],
    environment=SENTRY_ENV
)

# pylint: disable=redefined-outer-name
with configure_scope() as scope:
    scope.set_tag("microservice", "todo-service")
# pylint: enable=ungrouped-imports

# pylint: disable=invalid-name
app = Eve(auth=MyBasicAuth)
app.name = 'todo-service'
CORS(app)
Compress(app)
EveHealthCheck(app, '/healthcheck')

def adding_order(item):
    order = app.data.pymongo().db.todo.count_documents({}) + 1
    if not 'order' in item:
        item['order'] = order
    if not 'id' in item:
        item['id'] = order
    return item

def before_insert_todo(items):
    """ Before insert todo """
    for item in items:
        item = adding_order(item)

app.on_insert_todo += before_insert_todo
# app.on_updated_todo += after_updated_material

if __name__ == '__main__':
    app.logger.setLevel(logging.INFO)
    app.run(debug=True, host='0.0.0.0', port=8000)
