/// <reference types="Cypress" />

context('To Do create', () => {

  it('Field fields', () => {
    cy.visit('/')
    cy.wait(2000)
    cy.get("#inputRegister")
      .first()
      .focus()
      .type('Entregar projeto.')
      .type('{enter}')
    cy.get('body > div.q-notifications > div.q-notifications__list.q-notifications__list--bottom.fixed.column.items-center > div > div > div')
      .should('to.have.text', 'Item inserido com sucesso.')
  })

  it('Empty field', () => {
    cy.visit('/')
    cy.wait(2000)
    cy.get("#inputRegister")
      .first()
      .focus()
      .clear()
      .type('{enter}')
    cy.get('#registerItem > div > div.q-field__bottom.row.items-start.q-field__bottom--animated > div')
      .should('to.have.text', 'A descrição da tarefa não pode ser vazia.')
  })

})