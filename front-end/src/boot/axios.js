import axios from 'axios';
import VueDraggable from 'vue-draggable';

export default async ({ Vue }) => {
  Vue.use(VueDraggable);
  Vue.prototype.$axios = axios.create({
    baseURL: 'http://localhost:8000',
    // baseURL: window.localStorage.getItem('url'),
    headers: {
      Authorization: 'Basic YWRtaW46YWRtaW4=',
      'Cache-Control': 'no-cache',
    },
  });
};
